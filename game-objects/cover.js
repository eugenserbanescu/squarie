import BaseObject from './base-objects/base-object';

export default class CoverObject extends BaseObject {
  constructor(options) {
    super(options);
    this.canvas = document.createElement('canvas');
    this.canvas.height = this.height;
    this.canvas.width = this.width;
    this.ctx = this.canvas.getContext('2d');
    this.ctx.fillStyle = this.color;
    this.ctx.fillRect(0, 0, this.width, this.height);
  }
}

CoverObject.prototype.width = 99;
CoverObject.prototype.height = 9;
CoverObject.prototype.classNames = 'box';
CoverObject.prototype.type = 'cover';
CoverObject.prototype.color = '#3d3d3d';
