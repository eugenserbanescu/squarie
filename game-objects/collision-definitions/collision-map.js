function takeDamage(){
  this.remove();
  return {
    remove: this.getPosition()
  }
}

export default {
  player: {
    bullet:  takeDamage,
    bounds: () => {},
    cover: () => {}
  },
  bullet: {
    player: takeDamage,
    bullet: takeDamage,
    bounds: takeDamage,
    cover: takeDamage
  },
  cover: {
    player: () => {},
    bullet: () => {}
  }
}
