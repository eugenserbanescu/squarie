import AngleBased from './base-objects/angle-based';

export default class Bullet extends AngleBased {
  constructor(options) {
    if(typeof options.autoMove === 'undefined') {
      options.autoMove = true;
    }
    super(options);
  }
}

Bullet.prototype.maxSpeed = 40;
Bullet.prototype.width = 19;
Bullet.prototype.height = 19;
Bullet.prototype.classNames = 'bullet';
Bullet.prototype.type = 'bullet';
Bullet.prototype.color = 'green';
