import AngleBased from './base-objects/angle-based';

export default class Player extends AngleBased {
  constructor(options){
    super(options);
    this.canShoot = true;
    this.speed = 0;
    this.isAlive = true;
  }

  shoot() {
    const retVal = !!this.canShoot;
    if(this.canShoot) {
      this.canShoot = false;
      setTimeout(() => this.canShoot = true, this.cooldown);
    }
    return retVal;
  }

}

Player.prototype.type = 'player';
Player.prototype.cooldown = 500;
Player.prototype.width = 49;
Player.prototype.height = 49;
Player.prototype.classNames = 'box player';
Player.prototype.maxSpeed = 30;
Player.prototype.color = 'red';
