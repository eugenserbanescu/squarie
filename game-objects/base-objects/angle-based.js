import BaseObject from './base-object';

export default class AngleBased extends BaseObject {
  constructor(options) {
    super(options);

    this.angle = options.angle;
    this.shouldMove = options.autoMove || false;
  }

  getShouldMove() {
    return this.shouldMove;
  }

  setShouldMove(val) {
    this.shouldMove = !!val;
  }

  changeSpeed(timeElapsed) {
    this.speed = Math.round(timeElapsed * (this.maxSpeed / 100));
  }

  getAngle() {
    return this.angle;
  }

  setAngle(angle) {
    this.angle = angle;
  }

  getDesiredPosition() {
    var xDelta = this.speed * Math.round(Math.sin(this.angle));
    var yDelta = this.speed * Math.round(Math.cos(this.angle));

    var newX = [
      Math.round(this.position.x[0] + xDelta),
      Math.round(this.position.x[1] + xDelta)
    ];
    var newY = [
      Math.round(this.position.y[0] + yDelta),
      Math.round(this.position.y[1] + yDelta)
    ];
    return {
      x: newX,
      y: newY
    }
  }
}

AngleBased.prototype.static = false;
