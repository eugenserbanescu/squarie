import clone from '../../utils/clone';

export default class BaseObject {
  constructor(options) {
    const halfWidth = Math.floor(this.width / 2);
    const halfHeight = Math.floor(this.height / 2);
    this.id = options.id;
    this.position = {
      x: [options.startX - halfWidth, options.startX + halfWidth],
      y: [options.startY - halfHeight, options.startY + halfHeight]
    };
    this.canvas = document.createElement('canvas');
    this.canvas.height = this.height;
    this.canvas.width = this.width;
    this.ctx = this.canvas.getContext('2d');
    this.ctx.fillStyle = this.color;
    this.ctx.fillRect(0, 0, this.width, this.height);
  }

  getRedrawData() {
    return {
      canvas: this.canvas,
      position: {
        x: this.position.x[0],
        y: this.position.y[0]
      }
    }
  }

  show() {
    this.move(this.position);
  }

  move(newPosition){
    this.position = newPosition;
  }

  getPosition() {
    return clone(this.position);
  }

  getCentre() {
    return {
      x: this.position.x[0] + (Math.floor(this.position.x[1] - this.position.x[0]) / 2),
      y: this.position.y[0] + (Math.floor(this.position.y[1] - this.position.y[0]) / 2)
    }
  }
  isAlive: true
}

BaseObject.prototype.isAlive = true;
BaseObject.prototype.static = true;
