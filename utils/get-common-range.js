export default function getCommonRange(a,b) {
  const startA = a[0];
  const startB = b[0];
  const endA = a[1];
  const endB = b[1];

  const start = Math.max(startA, startB);
  const end = Math.min(endA, endB);
  const overlaps = end - start >= 0;

  return overlaps ? [start,end] : []
}
