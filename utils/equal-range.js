export default function equalRange(a,b) {
  return a[0] === b[0] && a[1] === b[1];
}
