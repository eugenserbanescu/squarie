export default function clone(item) {
  if(Array.isArray(item)) {
    return item.map(val => val);
  } else {
    return JSON.parse(JSON.stringify(item))
  }
}
