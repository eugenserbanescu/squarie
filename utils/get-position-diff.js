// Asssumes partially overlapping ranges
// Returns B range outside of A
export default function getPositionDiff(a,b){
  const startA = a[0];
  const startB = b[0];
  const endA = a[1];
  const endB = b[1];

  if(startB < startA) {
    return [startB, startA - 1];
  } else if(endA < endB) {
    return [endA + 1, endB];
  } else if (startA === startB && endA === endB) {
    return false;
  } else {
    return b;
  }
}
