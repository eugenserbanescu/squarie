export default function inRange(val, min, max){
  return val >= min && val <= max;
}
