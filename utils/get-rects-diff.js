import getPositionDiff from './get-position-diff';
import clone from './clone';

export default function getRectsDiff(a, b) {
  const xDiff = getPositionDiff(a.x, b.x);
  const yDiff = getPositionDiff(a.y, b.y);
  let diffRects = {
    x: false,
    y: false,
    corner: false
  };

  if(!xDiff && !yDiff) return false;

  if(xDiff) {
    diffRects.x = {
      x: xDiff,
      y: clone(a.y)
    }
    if(yDiff) {
      diffRects.corner = {
        x: xDiff,
        y: yDiff
      }
    }
  }
  if(yDiff) {
    diffRects.y = {
      x: clone(a.x),
      y: yDiff
    }
  }
  return diffRects;
}
