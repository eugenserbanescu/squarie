export default function canvasHandler(background) {
  const gameElement = document.querySelector('#game');
  const gameCtx = gameElement.getContext('2d');
  const width = gameElement.offsetWidth + 1;
  const height = gameElement.offsetHeight + 1;
  gameCtx.width = width;
  gameCtx.height = height;

  return {
    repaint: function(objects) {
      gameCtx.clearRect(0, 0, width, height);
      Object.keys(objects).forEach(key => {
        const data = objects[key].getRedrawData();
        gameCtx.drawImage(data.canvas, data.position.x, data.position.y)
      });
    }
  }
}
