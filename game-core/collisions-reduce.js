import clone from '../utils/clone';
const modifier = -1;

export default function reduceCollisions(currentPosition, axis) {
  return function reduceCollisionsThunk(currentSmallest, currentCollision) {
    let alteredCollision = clone(currentCollision);
    const movementPositive = currentPosition[axis][1] <= currentCollision.ranges[0][axis][0];
    const baseIndex = movementPositive ? 1 : 0;
    const otherIndex = movementPositive ? 0 : 1;

    let currentDistance = (currentPosition[axis][baseIndex] - currentCollision.ranges[0][axis][otherIndex]) * -1;
    const sign = Math.sign(currentDistance);
    currentDistance = (Math.abs(currentDistance) - 1) * sign;

    alteredCollision.distance = currentDistance;
    if (!currentSmallest) {
      return alteredCollision;
    } else {
      return currentDistance > currentSmallest.distance ? currentSmallest : alteredCollision;
    }
  }
}
