export default function chooseDistance (a, b, c,func) {
  const aDefined = typeof a !== 'undefined';
  const bDefined = typeof b !== 'undefined'
  if(aDefined && bDefined) return func(a, b);
  if(!aDefined && !bDefined) return c;
  if(aDefined) return a;
  if(bDefined) return b;
}
