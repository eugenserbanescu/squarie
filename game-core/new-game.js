import canvasHandler from './canvas';
import frameUpdater from './frame-updater';
import generateGameObjects from './generate-objects';
import Grid from './grid';
import MotionHandler from '../user-inputs/motion-handler';

export default function game(level) {
  const size = {}
  const grid = new Grid({
    width: 1001,
    height: 601
  });

  // init game objects
  let gameObjects = generateGameObjects();
  Object.keys(gameObjects).forEach(key => grid.addRect(gameObjects[key].getPosition(), gameObjects[key].id));

  // DIRTY FIXME: initialise keyboard listener
  let currentMotion = {
    x: 0,
    y: 0
  };

  const canvas = canvasHandler('#f2f2f2');

  // DIRTY FIXME: handling object adding here, since we're instantiating the listener here
  let objectsToAdd = [];
  let objectsToMove = [];

  const updateFrame = frameUpdater(grid, gameObjects, objectsToMove);

  // keyboard handler to change player ange and speed
  let motionHandler =  MotionHandler();

  function frameChecker() {
    console.time('frame');
    const player = gameObjects.player;
    player.changeAngle(motionHandler.getAngle());
    player.changeSpeed();
    if(motionHandler.getKeysPressed().length;) {
      objectsToMove.splice(objectsToMove.indexOf(player.id), 1);
    } else if (objectsToMove.indexOf(player.id) === -1) {
      objectsToMove.push(player.id);
    }

    updateFrame(objectsToAdd);

    canvas.repaint(gameObjects);
    console.timeEnd('frame');
    window.requestAnimationFrame(frameChecker);

  }

  frameChecker();

  return {
    getGrid: () => grid,
    getGameObjects: () => gameObjects
  }
}
