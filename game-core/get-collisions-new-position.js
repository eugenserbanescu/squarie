import chooseDistance from './choose-distance/';
import clone from '../utils/clone';
import getRectsDiff from '../utils/get-rects-diff';
import reduceCollisions from './collisions-reduce';

// TODO: look at prologue
export default function getCollisionsAndNewPosition(obj, grid) {
  const currentPosition = obj.getPosition();
  const desiredPosition = obj.getDesiredPosition();
  const desiredXOffset = desiredPosition.x[0] - currentPosition.x[0];
  const desiredYOffset = desiredPosition.y[0] - currentPosition.y[0];
  const potentialMovementAreas = getRectsDiff(clone(currentPosition), clone(desiredPosition));

  let xReduceFunc = reduceCollisions(currentPosition, 'x');
  let yReduceFunc = reduceCollisions(currentPosition, 'y');

  // get collisions per area
  const potentialCollisions = {}
  Object.keys(potentialMovementAreas).forEach(area => {
    if(!potentialMovementAreas[area]) {
      potentialCollisions[area] = [];
      return;
    }
    grid.checkRect(potentialMovementAreas[area], obj.id)
    potentialCollisions[area] = grid.getCollisions(true);
  });
  // if no collisions return desiredPos
  if(!Object.keys(potentialCollisions).reduce((prev, area) => prev + potentialCollisions[area].length, 0)){
    return {
      position: desiredPosition,
      collisions: []
    }
  }

  // get closest x, y and corner collisions
  const closestRanges = {
    x: potentialCollisions.x.reduce(xReduceFunc, false),
    y: potentialCollisions.y.reduce(yReduceFunc, false),
    corner: {
      x: potentialCollisions.corner.reduce(xReduceFunc, false),
      y: potentialCollisions.corner.reduce(yReduceFunc, false)
    }
  }

  // set delta x and delta y based on existing collisions
  let deltaX = chooseDistance(
    closestRanges.x.distance,
    closestRanges.corner.x.distance,
    desiredXOffset,
    (a,b) => Math.min(Math.abs(a),Math.abs(b)) * Math.sign(a)
  );
  let deltaY = chooseDistance(
    closestRanges.y.distance,
    closestRanges.corner.y.distance,
    desiredYOffset,
    (a,b) => Math.min(Math.abs(a),Math.abs(b)) * Math.sign(a)
  );

  // if both are zero
  if(deltaX === 0 && deltaY === 0) {
    if(closestRanges.x === false) {
      deltaX = desiredXOffset;
    } else if(closestRanges.x.distance !== 0) {
      deltaX = closestRanges.x.distance;
    } else if(closestRanges.y === false){
      deltaY = desiredYOffset;
    } else if(closestRanges.y.distance !== 0) {
      deltaY = closestRanges.y.distance;
    }
  }

  const finalCollisions = [
    closestRanges.x,
    closestRanges.y,
    closestRanges.corner.x,
    closestRanges.corner.y
  ].filter(c => !!c);

  const newPosition = {
    x: [currentPosition.x[0] + deltaX, currentPosition.x[1] + deltaX],
    y: [currentPosition.y[0] + deltaY, currentPosition.y[1] + deltaY]
  }

  xReduceFunc = yReduceFunc = null;

  return {
    position: newPosition,
    collisions: finalCollisions
  }

}
