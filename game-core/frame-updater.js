import collisionDefinitions from '../game-objects/collision-definitions/collision-map';
import getCollisionsAndNewPosition from './get-collisions-new-position';

export default function frameUpdater(grid, gameObjects, objectsToMove) {
  let markedToRemove = [];
  return function frameUpdaterThunk(objectsToAdd){
    // add new objects
    objectsToAdd.length && objectsToAdd.forEach(id => {
      const obj = gameObjects[id];
      const collisions = grid.checkRect(obj.getPosition(), id);
      if(collisions.length) {
        collisions.forEach(collision => {
          if(collision.inhabitantId === 'bounds') return;
          const inhabitant = gameObjects[collision.inhabitantId];
          collisionDefinitions[inhabitant.type][obj.type].call(inhabitant, collision);
        });
        return;
      } else {
        grid.addRect(obj.getPosition(), id);
      }
      if (obj.speed) {
        objectsToMove.push(obj);
      }
    });
    // move objects that want to move and handle their collisions
    objectsToMove.length && objectsToMove.forEach(id => {
      const obj = gameObjects[id];
      if(markedToRemove.indexOf(id) !== -1) return;
      const {position, collisions} = getCollisionsAndNewPosition(obj, grid);

      obj.move(position);
      grid.moveRect(position, id);
      // resolve all collisions
      collisions.forEach(collision => {
        if(collision.inhabitantId === 'bounds') return;
        const inhabitant = gameObjects[collision.inhabitantId];
        collisionDefinitions[inhabitant.type][obj.type].call(inhabitant, collision);
        collisionDefinitions[obj.type][inhabitant.type].call(obj, collision);
        !inhabitant.isAlive && markedToRemove.push(inhabitantId);
      });
    });
    // remove objects that died during collisions :D
    markedToRemove.length && markedToRemove.forEach(obj => {
      grid.removeRect(obj.getPosition(), obj.id);
      objectsToMove.splice(indexOf(obj.id), 1);
      delete gameObjects[obj.id];
    });

    markedToRemove = [];
  }

}
