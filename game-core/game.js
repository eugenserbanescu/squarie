import canvasHandler from './canvas';
import collisionDefinitions from '../game-objects/collision-definitions/collision-map';
import generateGameObjects from './generate-objects';
import getCollisionsAndNewPosition from './get-collisions-new-position';
import Grid from './grid';
import MotionHandler from '../user-inputs/motion-handler';
import Player from '../game-objects/player';

export default class Game {
  constructor(options) {
    this.grid = new Grid({
      width: 1001,
      height: 601
    });

    // generate game objects
    // TODO: maps at some point
    this.gameObjects = generateGameObjects();
    this.movingObjects = [];
    this.objectsToAdd = [];
    Object.keys(this.gameObjects).forEach(key => {
      const obj = this.gameObjects[key];
      if(!obj.static) {
        this.movingObjects.push(key);
      }
      this.grid.addRect(obj.getPosition(), key);
    });

    //add player
    this.player = this.gameObjects.player = new Player({
      startX: 250,
      startY: 35,
      id: 'player'
    });
    this.grid.addRect(this.player.getPosition(), this.player.id);

    // TODO: maps textures and such
    this.canvas = canvasHandler('#f2f2f2');
    // keyboard handler
    this.keyboardHandler = new MotionHandler();

    this.lastTimestamp = performance.now();

    this.gameLoop();
  }

  gameLoop(newTimestamp) {
    const timeElapsed = newTimestamp - this.lastTimestamp;
    this.lastTimestamp = newTimestamp;
    // handle player changes
    this.player.setAngle(this.keyboardHandler.getAngle());
    this.player.changeSpeed(timeElapsed);
    this.player.setShouldMove(this.keyboardHandler.getKeysPressed().length > 0);
    const playerMoving = this.movingObjects.indexOf(this.player.id) !== -1;
    if(this.player.getShouldMove()) {
      if(!playerMoving) {
        this.movingObjects.push(this.player.id);
      }
    } else {
      if(playerMoving) {
        this.movingObjects.splice(this.movingObjects.indexOf(this.player.id), 1);
      }
    }
    // add new objects
    this.checkAndAddObjects(this.objectsToAdd);
    this.objectsToAdd = [];
    // move everything, resolve collisions, repaint
    this.moveObjectsAndResolveCollisions();
    this.canvas.repaint(this.gameObjects);

    window.requestAnimationFrame(this.gameLoop.bind(this));
  }

  checkAndAddObjects(newObjects) {
    Object.keys(newObjects).forEach(key => {
      const obj = newObjects[key];
      this.grid.checkRect(obj.getPosition(), obj.id);
      const collisions = grid.getCollisions(true);
      if(collisions.length) {
        collisions.forEach(collision => {
          if(collision.inhabitantId === 'bounds') return;
          const inhabitant = this.gameObjects[collision.inhabitantId];
          collisionDefinitions[inhabitant.type][obj.type].call(inhabitant, collision);
        });
      } else {
        this.gameObjects[key] = obj;
        if(!obj.static) {
          this.movingObjects.push(id);
        }
      }
    });
  }

  moveObjectsAndResolveCollisions() {
    let markedToRemove = [];
    this.movingObjects.forEach(id => {
      if(markedToRemove.indexOf(id) !== -1) return;

      const obj = this.gameObjects[id];
      const {position, collisions} = getCollisionsAndNewPosition(obj, this.grid);
      obj.move(position);
      this.grid.moveRect(position, id);
      // resolve all collisions
      collisions.forEach(collision => {
        if(collision.inhabitantId === 'bounds') return;
        const inhabitant = this.gameObjects[collision.inhabitantId];
        collisionDefinitions[inhabitant.type][obj.type].call(inhabitant, collision);
        collisionDefinitions[obj.type][inhabitant.type].call(obj, collision);
        !inhabitant.isAlive && markedToRemove.push(inhabitantId);
      });
    });
    // remove objects that died during collisions :D
    markedToRemove.length && markedToRemove.forEach(obj => {
      this.grid.removeRect(obj.getPosition(), obj.id);
      this.movingObjects.splice(indexOf(obj.id), 1);
      delete this.gameObjects[obj.id];
    });
  }
}
