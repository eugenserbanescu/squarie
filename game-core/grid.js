import CollisionReporter from './collision-reporter';
import getCommonRange from '../utils/get-common-range';

//TODO: refactor: drop use of cells and use obj positions
//TODO: consider using collision areas (smaller grids) - register rects in smaller areas they're in for less checks
//TODO: return collisions as squares

export default function Grid(options) {
  let rects = {};
  let rectIds = [];
  let reporter = CollisionReporter();

  return {
    checkOutOfBounds(pos) {
      let outX = false;
      let outY = false;

      if(pos.x[0] < 1) {
        outX = [pos.x[0], 0]
      }
      if(pos.x[1] > options.width) {
        outX = [options.width, pos.x[1]]
      }

      if(pos.y[0] < 1) {
        outY = [pos.y[0], 0]
      }
      if(pos.y[1] > options.height) {
        outY = [options.height, pos.y[1]]
      }

      if(!outX && !outY) {
        return false;
      } else {
        return {
          x: outX ? outX : pos.x,
          y: outY ? outY : pos.y
        };
      }
    },

    checkRect: function(pos, id) {
      const boundsCollision = this.checkOutOfBounds(pos);
      if(boundsCollision) {
        reporter.addCollision({
          inhabitantId: 'bounds',
          intruderId: id,
          range: boundsCollision
        });
      }
      rectIds.forEach(objId => {
        if(objId === id) {
          return;
        }
        const object = rects[objId];
        const intersectX = getCommonRange(object.x, pos.x);
        const intersectY = getCommonRange(object.y, pos.y);

        if(intersectX.length && intersectY.length) {
          reporter.addCollision({
            inhabitantId: objId,
            intruderId: id,
            range: {
              x: intersectX,
              y: intersectY
            }
          });
        }
      });
    },

    getCollisions: function(reset) {
      return reporter.getCollisions(reset);
    },

    resetReporter: function() {
      reporter && reporter.reset();
    },

    modifyRect: function(pos, id) {
      rects[id] = pos;
      if(this.checkOutOfBounds(pos)) {
        console.error('out of bounds attempt:', pos);
      }
    },

    addRect: function(pos, id) {
      rects[id] = pos;
      if(rectIds.indexOf(id) === -1) rectIds.push(id);
      this.modifyRect(pos, id);
    },

    moveRect: function(dst, id) {
      rects[id] && this.modifyRect(dst, id);
    },

    removeRect: function(id) {
      delete rects[id];
      rectIds.splice(rectIds.indexOf(id), 1);
    },
    getRects() {
      return rects;
    },
    getIds() {
      return rectIds;
    }
  }
}
