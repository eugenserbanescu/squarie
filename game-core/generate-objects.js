import CoverObject from '../game-objects/cover';

export default function generateGame(){
  let objects = {};

  objects.cover = new CoverObject({
    startX: 200,
    startY: 150,
    id: 'cover'
  });

  return objects;
}

//
// var currentMotion = {
//   x: 0,
//   y: 0
// };
// var gameObjects = {};
// var objectsToMove = [];
//
// gameObjects.player = new Player({
//   startX: 250,
//   startY: 35,
//   id: 'player'
// });
//
// // grid.addRect(gameObjects.player.getPosition(),'player');
// // gameObjects.player.show();
//
// gameObjects['cover'] = new CoverObject({
//   startX: 200,
//   startY: 150,
//   id: 'cover'
// });
//
// grid.addRect(gameObjects['cover'].getPosition(),'cover');
// gameObjects['cover'].show();
