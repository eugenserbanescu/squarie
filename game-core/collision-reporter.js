import getUniqueId from '../utils/unique';
import equalRange from '../utils/equal-range';

export default function CollisionReporter() {
  var collisions = {};
  return {
    addCollision: function(collision) {
      var intruderId = collision.intruderId
      var inhabitantId = collision.inhabitantId;
      var id = getUniqueId();
      var preExisting = Object.keys(collisions).find(key => {
        return collisions[key].inhabitantId === inhabitantId && collisions[key].intruderId === intruderId;
      });
      if (preExisting){
        const rangeAlreadyRegistered = collisions[preExisting].ranges.find(range => equalRange(range.x, collision.range.x) && equalRange(range.y, collision.range.y));
        !rangeAlreadyRegistered && collisions[preExisting].ranges.push(collision.range);
      } else {
        collisions[id] = {
          id: id,
          intruderId,
          inhabitantId,
          ranges: [
            collision.range
          ]
        };
      }

    },
    hasCollitions: function() {
      return !!collisions.length;
    },
    getCollisions: function(reset) {
      const collisionIds = Object.keys(collisions);
      const retCollisions = collisionIds.length ? collisionIds.map(key => collisions[key]) : [];
      if(reset) collisions = {};
      return retCollisions;
    }
  }
}
