export default class MotionHandler {
  constructor (){
    this.acceptedKeys = [37, 38, 39, 40, 65, 68, 83, 87];
    this.left = [65, 37];
    this.right = [68, 39];
    this.up = [83, 40];
    this.down = [87, 38];
    this.currentlyPressed = [];
    this.angle;

    document.addEventListener('keydown', e => {
      const key = e.keyCode;
      if(this.acceptedKeys.indexOf(key) !== -1 && this.currentlyPressed.indexOf(key) === -1) {
        this.currentlyPressed.push(key);
      }
      this.angle = this.calculateAngle();
    });

    document.addEventListener('keyup', e => {
      const key = e.keyCode;
      if(this.currentlyPressed.indexOf(key) !== -1) {
        this.currentlyPressed.splice(this.currentlyPressed.indexOf(key), 1);
      }
      this.angle = this.calculateAngle();
    });
  }

  calculateAngle() {
    let x = 0;
    let y = 0;
    this.currentlyPressed.forEach(key => {
      if(this.left.indexOf(key) !== -1) {
        x -= 1;
      }
      if(this.right.indexOf(key) !== -1) {
        x += 1;
      }
      if(this.down.indexOf(key) !== -1) {
        y -= 1;
      }
      if(this.up.indexOf(key) !== -1) {
        y += 1;
      }
    });
    return Math.atan2((2 + x) - 2 , (2 + y) - 2);
  }
  getKeysPressed() {
    return this.currentlyPressed;
  }
  getAngle() {
    return this.angle;
  }
}
