export function ClickHandler(options) {
  document.body.addEventListener('mousedown', function(e) {
    e.preventDefault();
    var userCoords = {
      x: e.clientX,
      y: e.clientY
    }
    options.clickCallback(userCoords);
  });
}



export function AimHandler(options) {
  var timeout;
  function onMouseMove(e) {
    var userCoords = {
      x: e.clientX,
      y: e.clientY
    }
    console.log('aim',userCoords);
  }
  document.body.addEventListener('mousemove', function(e){
    if(!timeout) {
      timeout = setTimeout(function(){
        onMouseMove(e)
        clearTimeout(timeout);
        timeout = false;
      }, 100);
    }
  });
}

export default { ClickHandler, AimHandler };
